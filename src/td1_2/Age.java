package td1_2;

public class Age {
	
	
	public static void main(String[] args) {
		
		String nom = "Bob";
		int age = 18;
		double poids = 58;
		
		System.out.println(nom + " " + "a" + " " + age + " " + "ans");
		System.out.println(nom + " " + "pese" + " " + poids + " " + "kilos");
	}

}
