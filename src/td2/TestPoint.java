package td2;

public class TestPoint {

	
	public static void main(String[] args) {
		
		 Point p;
		 p = new Point(7,2);
		 
		 System.out.println("x = " + p.getX());
		 System.out.println("y = " + p.getY());
		 
		 // Exercice 1.3
		 
		 p.setX(5);
		 System.out.println("x = " + p.getX());
		 
		 p.setY(3);
		 System.out.println("y = " + p.getY());

		 // Exercice 1.8
		 
		 p.deplacer(-10, 7);
		 System.out.println("x = " + p.getX());
		 System.out.println("y = " + p.getY());
		 
	}

}
