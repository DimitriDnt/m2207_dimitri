package tp4;

public class Pokemon {
	
	// Attributs
	
	private int energie;
	private int maxEnergie;
	private String nom;
	private int puissance;
	
	// Accesseurs
	
	public String getNom() {
		
		return nom;
	}

	public int getEnergie() {
		
		return energie;
	}
	
	public int getPuissance() {
		
		return puissance;
	}
	
	// Constructeur
	
	public Pokemon(String n) {
		
		nom = n;
		maxEnergie = (50 + (int) (Math.random() * (90-50) + 1));
		energie = (30 + (int) (Math.random() * (maxEnergie - 30) + 1));
		puissance = (3 + (int) (Math.random() * (10 - 3) + 1));
	} 
	
	// Methodes
	
	public void sePresenter() {
		
		System.out.println("Je suis " + getNom() + ", j'ai " + getEnergie() + " points d'energie " +  "(" + maxEnergie + " max" + ")" + " et une puissance de " + getPuissance());
	}
	
	public void manger() {
		
		if (energie > 0) {
		
		energie = energie + (10 +(int) Math.random() * (30-10) + 1);
		}
		
		if (energie > maxEnergie) {
			
			energie = maxEnergie;
		}
	}
	
	public void vivre() {
		
		energie = energie - (20 + (int) (Math.random() * (40-20) + 1));
		
		if (energie < 0) {
			
			energie = 0;
		}
	}
	
	public boolean isAlive() {
		
		if (energie > 0) {
			
			return true;
		}
		
		else {
			
			return false;
		}
		
	}
	
	public void cycleVie() {
		
		int compteur = 0;
		
		while (energie > 0) {
			
			vivre();
			manger();
			compteur = compteur +1;
		}
		
		System.out.println(getNom() + " a vecu " + compteur + " cycles");
	}
	
	public void perdreEnergie(int perte) {
		
		if (energie < ((maxEnergie * 25)/100)) {
			
			perte = perte * (int) 1.5;
		}
		
		if (perte > energie) {
			
			energie = 0;
		}
		
		else {
			
		energie = energie - perte;
		}
		
	}
	
	public void attaquer(Pokemon adversaire) {
		
		if (energie < ((maxEnergie * 25)/100)) {
			
			puissance = (2 *  puissance);
			System.out.println(getNom() + " est en mode furie !");
		}
		
		adversaire.perdreEnergie(getPuissance());
		int fatigue = ((0 + (int) (Math.random() * (1-0) + 1)));
		puissance = puissance - fatigue;
		
		
		if (puissance < 0) {
			
			puissance = 1;
		}
		
	}
}

