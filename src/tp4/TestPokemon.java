package tp4;

public class TestPokemon {

	public static void main(String[] args) {
		
		// Exercice 1.4
		
		Pokemon pokemon1 = new Pokemon("Magigwak");
		pokemon1.sePresenter();
		
		// Le pokemon mange et gagne de l'energie
		
		pokemon1.manger();
		pokemon1.sePresenter();
		
		// Le pokemon vit et perd de l'energie
		
		pokemon1.vivre();
		pokemon1.vivre();
		pokemon1.sePresenter();
		
		System.out.println("Actuellement, " + pokemon1.getNom() + " est " + pokemon1.isAlive());
		
		// Exercice 1.8 : Cycles
		
		Pokemon pokemon2 = new Pokemon("Kappachu");
		pokemon2.sePresenter();
		pokemon2.cycleVie();

	}

}
