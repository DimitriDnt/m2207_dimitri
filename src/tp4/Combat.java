package tp4;

public class Combat {


	public static void main(String[] args) {
		
		Pokemon pokemon1 = new Pokemon("Carapocebleu");
		Pokemon pokemon2 = new Pokemon("Cheumnipan");
		int nbRound = 0;
		
		pokemon1.sePresenter();
		pokemon2.sePresenter();
		
		
		while (pokemon1.getEnergie() > 0 && pokemon2.getEnergie() > 0) {
			
			nbRound = nbRound + 1;
			pokemon1.attaquer(pokemon2);
			pokemon2.attaquer(pokemon1);
			System.out.println("Round " + nbRound + " - " + pokemon1.getNom() + " : " + "(en)" + pokemon1.getEnergie() + " " + "(atk)" + pokemon1.getPuissance() + " " + pokemon2.getNom() + " : " + "(en)" + pokemon2.getEnergie() + " " + "(atk)" + pokemon2.getPuissance());
			
		}
		
		if (pokemon1.getEnergie() > 0) {
			
			System.out.println(pokemon1.getNom() + " gagne en " + nbRound + " rounds");
		}
		
		if (pokemon2.getEnergie() > 0) {
			
			System.out.println(pokemon2.getNom() + " gagne en " + nbRound + " rounds !");

		}
		
		if (pokemon1.getEnergie() == 0 && pokemon2.getEnergie() == 0) {
			
			System.out.println("Match nul !");

		}
	}

}
