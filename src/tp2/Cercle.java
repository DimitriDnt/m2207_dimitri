package tp2;

public class Cercle extends Forme {
	
	// Attribut
	
	protected double rayon;
	
	// Constructeur
	
	public Cercle() {
		
		super(); // Renvoie au constructeur de la classe mere Forme
		rayon = 1.0;
	}
	
	public Cercle(double r) {
		
		rayon = r;
	}
	
	public Cercle(double r, String couleur, boolean coloriage) {
		
		super(couleur, coloriage);
		rayon = r;
	}
	
	// Accesseurs
	
	public double getRayon() {

		return rayon;
		
	}
	
	public void setRayon(double ra) {
		
		rayon = ra;
		
	}
	
	// Methodes
	
	public String seDecrire() { // Exercice 2.6
		
		return "Un Cercle de rayon " + rayon + " " + "est issue d'" + super.seDecrire();

	}
	
	public double calculerAire() {
		
		return ((rayon*rayon)*Math.PI);
	}
	
	public double calculerPerimetre() {
		
		return (2*Math.PI*rayon);
	}

}
