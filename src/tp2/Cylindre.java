package tp2;

public class Cylindre extends Cercle {
	
	// Attribut
	
	private double hauteur;
	
	// Constructeurs
	
	public Cylindre() {
		
		hauteur = 1.0;
	}
	
	public Cylindre(double h, double rayon, String couleur, boolean coloriage) {
		
		super(rayon, couleur, coloriage);
		hauteur = h;
		
	}
	
	// Accesseurs
	
	public double getHauteur() {
		
		return hauteur;
	}
	
	public void setHauteur(double h) {
		
		hauteur = h;
	}
	
	// Methodes
	
	public String seDecrire() {
		
		return "Un Cylindre de hauteur " + hauteur + " " + "est issue d'" + super.seDecrire();
		
	}
	
	public double calculerVolume() {
		
		return (Math.PI*(rayon*rayon)*hauteur);
				
	}

}
