package tp2;

public class TestForme {

	public static void main(String[] args) {
		
		Forme f1, f2;
		f1 = new Forme();
		f2 = new Forme("vert", false);
		
		System.out.println("f1 :" + f1.getCouleur() + "-" + f1.isColoriage());
		System.out.println("f2 :" + f2.getCouleur() + "-" + f2.isColoriage());


		// Exercice 1.5
		
		f1.setCouleur("rouge");
		f1.setColoriage(true);
		System.out.println("f1 :" + f1.getCouleur() + "-" + f1.isColoriage());
		
		// Exercice 1.7
		
		System.out.println(f1.seDecrire());
		
		// Exercice 2.4
		
		Cercle c1;
		c1 = new Cercle();
		
		// Exercice 2.5 : Utilisation des accesseurs
		
		System.out.println("Un Cercle de rayon " + c1.getRayon() + " " + "est issue d'une Forme de couleur " + c1.getCouleur() + " " + "et de coloriage" + " " + c1.isColoriage());

		// Exercice 2.6 : Utilisation de la methode seDecrire
		
		System.out.println(c1.seDecrire());
		
		// Exercice 2.8 : Creation et affichage de c2
		
		Cercle c2;
		c2 = new Cercle(2.5);
		
		System.out.println(c2.seDecrire());
		
		// Exercice 2.10 : Creation et affichage de c3
		
		Cercle c3;
		c3 = new Cercle(3.2,"jaune",false);
		
		System.out.println(c3.seDecrire());
		
		// Exercice 2.11 : Calcul de l'aire et du perimetre des cercles 1 et 2
		
		System.out.println("L'aire de c2 est : " + c2.calculerAire());
		System.out.println("Le perimetre de c2 est : " + c2.calculerPerimetre());

		System.out.println("L'aire de c3 est : " + c3.calculerAire());
		System.out.println("Le perimetre de c3 est : " + c3.calculerPerimetre());
		
		// Exercice 3.2 : Description et cr�ation du cylindre 1
		
		Cylindre cy1;
		cy1 = new Cylindre();
		
		System.out.println(cy1.seDecrire());
		
		// Exercice 3.4 : Description et cr�aton du cylindre 2
		
		Cylindre cy2;
		cy2 = new Cylindre(4.2, 1.3, "bleu", true);
		
		System.out.println(cy2.seDecrire());
		
		// Exercice 3.5 : Calcul et affichage du volume des cylindres 1 et 2
		
		System.out.println("Le volume de cy1 est : " + cy1.calculerVolume());
		System.out.println("Le volume de cy2 est : " + cy2.calculerVolume());

	}

}
