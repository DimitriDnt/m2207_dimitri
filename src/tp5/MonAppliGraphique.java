package tp5;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

public class MonAppliGraphique extends JFrame{
	
	// Attributs
	JButton b1;
	JButton b2;
	JButton b3;
	JButton b4;
	JButton b5;
	JLabel monLabel;
	JTextField monTextField;
	
	// Constructeur
	
	public MonAppliGraphique() {
		
		super();
		this.setTitle("Ma première fenetre");
		this.setSize(400,200);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fenetre est creee !");
		Container panneau = getContentPane();
		// La taille et l'emplacement du bouton ont changé
		//panneau.setLayout(new FlowLayout());
		panneau.setLayout(new GridLayout(3, 2));
		// Creation des boutons
		b1 = new JButton("Bouton 1");
		b2 = new JButton("Bouton 2");
		b3 = new JButton("Bouton 3");
		b4 = new JButton("Bouton 4");
		b5 = new JButton("Bouton 5");
		// Ajout des boutons dans le container
		panneau.add(b1);
		panneau.add(b2, BorderLayout.NORTH);
		panneau.add(b3, BorderLayout.EAST);
		panneau.add(b4, BorderLayout.SOUTH);
		panneau.add(b5, BorderLayout.WEST);
		// Creation d'un Label et d'un TextField
		//monLabel = new JLabel("Je suis un JLabel");
		//monTextField = new JTextField("Je suis un JTextField");
		//panneau.add(monLabel);
		//panneau.add(monTextField);
		this.setVisible(true);
	}
	
	
	// Methodes
	
	
	// Methodes Main
	
	public static void main(String[] args) {
		
		MonAppliGraphique app = new MonAppliGraphique () ;
	}
	
}
