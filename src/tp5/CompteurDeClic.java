package tp5;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

public class CompteurDeClic extends JFrame implements ActionListener {
	
	// Attributs
	
	JButton bouton;
	JLabel monLabel;
	int compteur;
	
	// Constructeur
	
	public CompteurDeClic() {
		
		super();
		this.setTitle("Compteur de clic");
		this.setSize(200,100);
		this.setLocation(1000,500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fenetre est creee !");
		Container panneau = getContentPane();
		panneau.setLayout(new FlowLayout());
		bouton = new JButton("Click !");
		panneau.add(bouton);
		monLabel = new JLabel("Vous avez cliqué 0 fois");
		panneau.add(monLabel);
		bouton.addActionListener(this);
		this.setVisible(true);
		
	}
	
	// Methode
	
	public void actionPerformed(ActionEvent e) {
		
		System.out.println("Action detected");
		compteur = compteur+1;
		monLabel.setText("Vous avez cliqué " + compteur + " fois");
	}
	

	// Methode main
	
	public static void main(String[] args) {

		CompteurDeClic app = new CompteurDeClic ();
	}
	
}