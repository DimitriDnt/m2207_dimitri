package tp5;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

public class PlusOuMoinsCher extends JFrame implements ActionListener {
	
	// Attributs
	
	JButton bouton;
	JLabel label1, label2;
	JTextField texte;
	int nombreMystere = (int) (Math.random() * 100 + 1);
	int c;
	
	// Constructeur
	
	public PlusOuMoinsCher() {
		
		super();
		this.setTitle("Plus cher ou moins cher ?");
		this.setSize(350,150);
		this.setLocation(1000,500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("Le nombre est : " + nombreMystere);
		Container panneau = getContentPane();
		panneau.setLayout(new GridLayout(2, 2));
		label1 = new JLabel(" Votre proposition :");
		panneau.add(label1);
		texte = new JTextField();
		panneau.add(texte);
		bouton = new JButton("Vérifier");
		panneau.add(bouton);
		label2 = new JLabel(" La réponse");
		panneau.add(label2);
		bouton.addActionListener(this);
		this.setVisible(true);

	}

	// Methode
	
	public void actionPerformed(ActionEvent e) {
		
		System.out.println(texte.getText());
		c = Integer.parseInt(texte.getText());
		
		if (c > nombreMystere) {
			
			label2.setText("Moins cher !");
		}
		
		if (c < nombreMystere) {
			
			label2.setText("Plus cher !");
		}
		
		if (c == nombreMystere) {
			
			label2.setText("Vous avez deviné !");
		}
		
	}
	
	public void init() {
		
		nombreMystere = (int) (Math.random() * 100 + 1);
	}
	
	// Methode main
	
	public static void main(String[] args) {
		
		PlusOuMoinsCher app = new PlusOuMoinsCher();
	}

}
