package tp1;

// Exercice 2.3

public class MaBanque {

	
	public static void main(String[] args) {
		
		Compte compte;
		compte = new Compte(1);
		
		System.out.println(" Solde initial = " + compte.getSolde());

		System.out.println(" Decouvert = " + compte.getDecouvert());
		
		compte.depot(500);
		System.out.println(" Solde apres depot = " + compte.getSolde());

		// Exercice 2.5
		
		System.out.println(compte.retrait(100));
		System.out.println(" Nouveau solde = " + compte.getSolde());
		
		
		// Exercice 2.6
		
		Compte compte2;
		compte2 = new Compte(2);

		compte2.depot(1000);
		compte2.afficherSolde();
		
		System.out.println(compte2.retrait(600));
		compte2.afficherSolde();
		
		System.out.println(compte2.retrait(700));
		compte2.afficherSolde();
		
		compte2.setDecouvert(500);
		System.out.println(compte2.retrait(700));
		compte2.afficherSolde();

		// Exercice 3.4 : Cr�ation d'un client ayant le compte2
		
		Client client1;
		client1 = new Client("Bill", "Gates", compte2);
		
		client1.afficherSolde();

	}

}

