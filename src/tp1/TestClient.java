package tp1;

public class TestClient {

	public static void main(String[] args) {
		
		Compte compte1;
		compte1 = new Compte(1);
		
		Compte compte2;
		compte2 = new Compte(2);
		
		Client client; 
		client = new Client("Bill", "Gates", compte1);
		
		
		System.out.println("Nom = " + client.getNOM());
		System.out.println("Prenom = " + client.getPRENOM());
		
		client.afficherSolde();
		
		
		ClientMultiComptes client1;
		client1 = new ClientMultiComptes("Dimitri", "Dennemont", compte2);
		
		client1.ajouterCompte(compte1);
		
	}

}

