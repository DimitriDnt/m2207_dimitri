package tp1;

// Exercice 2.1

public class Compte {
	
	//Attributs
	
	private int numero;
	private double solde;
	private double decouvert;
	
	//Constructeur
	
	public Compte(int numero) {
		
		solde = 0;
		decouvert = 100;
	}
	
	//Accesseurs
	
	// Exercice 2.2
	
	public void setDecouvert(double montant) {
		
		decouvert = montant;
				
	}
	
	public double getDecouvert(){
		
		return decouvert;
	}

	public int getNumero() {
		
		return numero;
	}
	
	public double getSolde() {
		
		return solde;
		
	}
	
	
	//Methode
	
	public void afficherSolde(){
		
		System.out.println(solde);
		
	}
	
	// Exercice 2.4

	public void depot(double montant) {
		
		solde = solde + montant;
		
	}
	
	// Exercice 2.5
	
	public String retrait(double montant) {
		
		if (montant > (solde + decouvert)) {
			
			return "Retrait refuse";
	}
		else {
			
			solde = solde - montant;
			return "Retrait effectue";
		}
	}
	
}
