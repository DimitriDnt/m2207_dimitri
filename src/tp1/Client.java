package tp1;

// Exercice 1.1

public class Client {

	// Attributs
	
	public String prenom;
	public String nom;
	private Compte comptecourant; // Exercice 3.1
	
	// Constructeur
	
	// Exercice 1.2
	
	public Client(String n, String p, Compte c) { // Exercice 3.2
		
		nom = n;
		prenom = p;
		comptecourant = c;
	}
	
	// Accesseurs
	
	public String getNOM() {
		
		return nom;
		
	}
	
	public String getPRENOM() {
		
		return prenom;
		
	}
	
	public double getSolde(double solde) {
		
		return solde;
		
	}
	
	// Methodes
	
	public void afficherSolde() {
		
		System.out.println(comptecourant.getSolde());
		
		
	}
}
