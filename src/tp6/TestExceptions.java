package tp6;

public class TestExceptions {

	// Methode main
	public static void main(String[] args) { 
		int x = 2, y = 0;

		try{
			System.out.println("y/x = " + y/x); 
			System.out.println("x/y = " + x/y); 
			System.out.println("Commande de fermeture du programme");
		}
		// 

		catch (Exception e){
			System.out.println("Une exception a ete capturee");
			// Suite a la  modification du programme, il capture une exception
		}

		System.out.println("Fin du programme");

		// Il y avait une erreur d'execution car on tente de diviser 2 par 0.
	}
}