package tp6;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.net.ServerSocket;
import java.net.Socket;

public class PanneauServeur extends JFrame implements ActionListener  {

	// Attributs

	JButton bouton;
	JTextArea texte;
	JScrollPane scroll;
	ServerSocket serveur;

	// Constructeur

	public PanneauServeur() {

		super();
		this.setTitle("Serveur - Panneau d'affichage");
		this.setSize(400,300);
		this.setLocation(1000,500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container panneau = getContentPane();
		bouton = new JButton("Exit");
		panneau.add(bouton, BorderLayout.SOUTH);
		texte = new JTextArea();
		scroll = new JScrollPane(texte);
		panneau.add(scroll);
		bouton.addActionListener(this);
		texte.append("Le panneau est actif");
		this.setVisible(true);

	}
	// Methodes

	public void actionPerformed(ActionEvent e) {

		System.exit(-1);
	}

	// Methode main

	public static void main(String[] args) {

		PanneauServeur app = new PanneauServeur ();
	}

}
