package tp3;

public class Histoire {

	public static void main(String[] args) {
		
		// Exercice 1.7 : Creation et presentation d'un humain
		
		Humain humain1 = new Humain("Bernard");
		humain1.sePresenter();
		humain1.boire();
		
		// Exercice 2.4 : Kidnapping et sauvetage d'une Dame
		
		Dame dame1 = new Dame("Jacqueline");
		dame1.sePresenter();
		dame1.priseEnOtage();
		dame1.estLiberee();
		
		// Creation et presentation d'un brigand
		
		Brigand brigand1 = new Brigand("Gunther");
		brigand1.sePresenter();
		
		// Creation et presentation d'un cowboy
		
		Cowboy cowboy1 = new Cowboy("Simon");
		cowboy1.sePresenter();
				
		// Exercice 7.5 : Scenario
		
		System.out.println("\nExercice 7.5");
		dame1.sePresenter();
		brigand1.sePresenter();
		brigand1.enleve(dame1);
		brigand1.sePresenter();
		dame1.sePresenter();
		cowboy1.tire(brigand1);
		dame1.estLiberee();
		dame1.sePresenter();
		
		// Exercice 8.1 : Le Sherif
		
		Sherif sherif1 = new Sherif("Marshall");
		sherif1.sePresenter();
		
		// Exercice 8.4 : Le Sherif arrete un brigand
		
		System.out.println("\nExercice 8.4");
		sherif1.sePresenter();
		sherif1.coffrer(brigand1);
		brigand1.emprisonner(sherif1);
		sherif1.sePresenter();
		

		// Exercice 9 : Sherif = Cowboy
		
		Cowboy clint = new Sherif("Clint");
		clint.sePresenter();
		
		// On peut faire appel a toutes les methodes de la classe Cowboy
		// Ce cowboy ne peut pas coffrer un brigand car c'est une methode de la classe Sherif
		
		
	}

}
