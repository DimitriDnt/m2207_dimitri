package tp3;

public class Sherif extends Cowboy {
	
	// Attribut
	
	private int nbBrigand;
	
	// Constructeur
	
	public Sherif (String n) {
		
		super(n);
		nbBrigand = 0;
	}
	
	public String quelEstTonNom() {
		
		return "Sherif " + nom;
		
	}
	
	public void sePresenter() {
		
		super.sePresenter();
		parler("Actuellement, j'ai capture " + nbBrigand + " brigand(s).");
	}
	
	public void coffrer(Brigand b) {
		
		nbBrigand = nbBrigand + 1;
		parler(quelEstTonNom() + " : Au nom de la loi, je vous arrete, " + b.quelEstTonNom() + " !");
		
	}

}
