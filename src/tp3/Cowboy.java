package tp3;

public class Cowboy extends Humain {
	
	// Attribut
	
	private int popularite;
	private String caracteristique;
	
	
	// Constructeur 
	
	public Cowboy(String n) {
		
		super(n);
		popularite = 0;
		boisson = "whiskey";
		caracteristique = "vaillant";
		
	}
	
	// Methode
		
	public void tire(Brigand brigand) {
		
		parler("Le " + caracteristique + " " + quelEstTonNom() + " tire sur " + brigand.quelEstTonNom() + " . PAN !");
		parler(quelEstTonNom() + " : Prend �a, voyou !");
		
	}
	
	public void libere(Dame dame) {
		
		dame.estLiberee();
		popularite = popularite + 10;
		
	}

}
