package tp3;

public class Dame extends Humain {
	
	// Attribut
	
	private boolean libre;
	
	// Constructeur
	
	public Dame (String n) {
		
		super(n);
		boisson ="Martini";
		libre = true;
	}
	

	// Methodes
	
	public void priseEnOtage() {
		
		libre = false;
		parler(nom + " : " + "Au secours !");
	}
	
	public void estLiberee() {
		
		libre = true;
		parler(nom + " : " + "Merci Cowboy.");
	}
	
	public String quelEstTonNom() {
		
		return "Miss " + nom;
	}
	
	public void sePresenter(){
		
		super.sePresenter();
		parler("Actuellement, je suis " + libre);
	}


}
