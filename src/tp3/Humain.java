package tp3;

public class Humain {
	
	// Attributs
	
	protected String nom;
	protected String boisson;
	
	// Constructeurs
	
	public Humain(String n){
		
		nom = n;
		boisson ="lait";
		
	}
	
	// Methodes
	
	public void parler(String texte){
		
		System.out.println(texte);
	}
	
	public String quelleEstTaBoisson() {
		
		return boisson;
	}
	
	public String quelEstTonNom() {
		
		return nom;
	}
	
	public void sePresenter() {
		
		parler((quelEstTonNom()) + " :" + " Bonjour, je suis " + quelEstTonNom() + " et ma boisson preferee est le " + quelleEstTaBoisson());
		
	}
	
	public void boire(){
		
		parler((quelEstTonNom()) + " :" + " Ah ! Un bon verre de " + quelleEstTaBoisson() + " !" + " GLOUPS !");
		
	}

}
