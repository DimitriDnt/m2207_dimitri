package tp3;

public class Brigand extends Humain {
	
	// Attributs
	
	private String look;
	private int nbDames, recompense;
	private boolean prison;
	
	// Constructeur
	
	public Brigand(String n) {
		
		super(n);
		look = "mechant";
		prison = false;
		recompense = 100;
		nbDames = 0;
		boisson = "cognac";
		
	}
	
	// Methodes
	
	public int getRecompense() {
		
		return recompense;
	}
	
	public String quelEstTonNom() {
		
		return nom + " le " + look;
	}
	
	public void sePresenter() {
		
		super.sePresenter();
		parler("J'ai l'air " + look + " et j'ai enleve " + nbDames + " dames");
		parler("Ma tete est mise a  prix " + recompense + "$ !!");
	}
	
	public void enleve(Dame dame) {
		
		nbDames = nbDames+1;
		recompense = recompense + 100;
		parler(quelEstTonNom() + " : Ah ah ! " + dame.quelEstTonNom() + ", tu es ma prisonniere !");
		dame.priseEnOtage();

		
	}
	
	public int nbDames() { // Rajout pour afficher le nombre de dames enlevees
		
		return nbDames;
	}
	
	public void emprisonner (Sherif s) {
		
		parler(quelEstTonNom() + " : Damned, je suis fait ! " + s.quelEstTonNom() + " , tu m'as eu !");
	}
	

}
