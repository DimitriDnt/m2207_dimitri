package td3;

public class TestAnimal {

	public static void main(String[] args) {
		
		Animal animal;
		animal = new Animal();
		
		System.out.println(animal.toString());
		
		System.out.println(animal.affiche());
		System.out.println(animal.cri());
		
		// Exercice 2.2
		
		Chien chien;
		chien = new Chien();
		
		System.out.println(chien.affiche());
		System.out.println(chien.cri());
		
		Chat chat;
		chat = new Chat();
		
		System.out.println(chat.affiche());
		System.out.println(chat.miauler());
		
		// Exercice 2.7
		System.out.println(animal.origine());
		System.out.println(chien.origine());
			
		System.out.println(animal.cri());
		System.out.println(chien.cri());
		System.out.println(chat.cri());
		System.out.println(chat.miauler());
		System.out.println(animal.miauler());


	}

}
