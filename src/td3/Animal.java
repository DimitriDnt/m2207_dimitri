package td3;

public class Animal {
	
	public String affiche() {
		
		return "Je suis un animal";
		
	}
	
	public String cri() {
		
		return "...";

	}
	
	// Exercice 2.7
	
	public final String origine() {
		
		return "La classe Animal est la mère de toutes les classes !";
	}
	
	public String miauler() {
		
		return "Miaou !";
		
	}
}

